Document: cddlibman
Title: cddlib Reference Manual
Abstract: This is a reference manual for cddlib-094. The manual describes the library
 functions and data types implemented in the cddlib C-library which is to perform
 fundamental polyhedral computations such as representation conversions and linear
 programming in both floating-point and GMP rational exact arithmetic. Please read
 the accompanying README file and test programs to complement the manual.
 .
 The new functions added in this version include dd MatrixCanonicalize to find a non-
 redundant proper H- or V-representation, dd FindRelativeInterior to find a relative
 interior point of an H-polyhedron, and dd ExistsRestrictedFace (Farkas-type
 alternative theorem verifier) to check the existence of a point satisfying a
 specified system of linear inequalities possibly including multiple strict
 inequalities.
 .
 The new functions are particularly important for the development of related software
 packages MinkSum (by Ch. Weibel) and Gfan (by Anders Jensen).
Section: Science/Mathematics

Format: PDF
Index: /usr/share/doc/libcdd-dev/cddlibman.pdf.gz
Files: /usr/share/doc/libcdd-dev/cddlibman.pdf.gz
